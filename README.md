# Frontend Applicant Test
---
##### Thoughts
1. Chrome browser will return 'cross-origin error' when you try to load a local JSON file, so I use my built in Python server to run localhost in this situation during development. If you have Python installed, or MAMP, or XAMP, or something similar that hosts on localhost, viewing the project that way would be ideal. Or on a Firefox browser, as its security is more slack than Chrome on cross-origin scenarios.
2. If I had more time I would write OOP rather than procedural Javascript, but I'm faster with procedural with Javascript.
3. I'm excited about the design, layout, and functionality that was implemented. If I made one more fundamental change it would be to make the main graph more responsive to small screens. The sub graph is responsive, and so is everything else.
4. Spent about 5 hours on and off, creating BB account, conceptualizing and implementing.




##### Instructions
1. Write a web application that reads the included `data.json` file and generates a list of bar graphs similar to the included screenshots.
2. A dropdown must be added at the top of the page. It must list all of the countries from the `data.json` file. Selecting one will limit the display to that country's data.
3. Fork this repository.
4. When you're done, send the link of your repository to your examiner for evaluation.

##### Goal
The goal is to demonstrate proficiency in web development, using Angular 1.x as the choice of Javascript framework.

##### Rules
* The only dependency that you're allowed to use is angular (`npm install angular`).
* You can use as many `dev-dependencies` as you see fit.
* Use of CSS preprocessors is highly encouraged, but not required.
* Your application must have an `index.html` and a `dist` directory. All files required by `index.html` to run must be in dist.
* Your application must work properly on both Chrome and Firefox.
