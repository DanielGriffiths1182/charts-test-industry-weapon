/*
    Global Variables

    NOTE Ideally I would not use procedual writing, but it's quicker for me.
*/
var labels = [];
var datas = [];
var category = 'horizontalBar';
var main = document.getElementById("bar-chart-horizontal");
var sub = document.getElementById("bar-chart");
var button = document.getElementById("button-back");
/*
    Callback function for loading JSON

    NOTE Chrome gives "cross-origin" error when loading local JSON, I personally used a Python3.6 server and localhost for the Dev Env.
    CLI = charts-test-industry-weapon $ python3 -m http.server 8000
*/
function loadJSON(callback) {
  var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
  xobj.open('GET', 'data.json', true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}
/*
    Function for back button
*/
function goBack(value) {
  if (main.style.display === 'none') {
      sub.style.display = 'none';
      button.style.display = 'none';
      main.style.display = 'block';
  }
  else {
    false;
  }
}
/*
    On dropdown select this function builds the secondary (sub) graph for specific nations
*/
function reportFunction(value) {
   var label_index = labels.indexOf(value);
   var bottom_data_slice = label_index * 2;
   var top_data_slice = bottom_data_slice + 2;
   var data_selection = datas.slice(bottom_data_slice, top_data_slice);
   var text_label_specific = 'Channels and Devices for ' + value;

    if (main.style.display === 'block') {
        main.style.display = 'none';
        button.style.display = 'block';
        sub.style.display = 'block';
    }
  /*
      Sub vertical bar graph from ChartJS class, implementing data we organize above
  */
   new Chart(document.getElementById("bar-chart"), {
       type: 'bar',
       data: {
         labels: [value + " devices", value + " channels"],
         datasets: [
           {
             label: text_label_specific,
             backgroundColor: ["rgba(55, 160, 225, 0.7)", "rgba(225, 58, 55, 0.7)"],
             hoverBackgroundColor: ["rgba(55, 160, 225, 0.7)", "rgba(225, 58, 55, 0.7)"],
             hoverBorderWidth: 2,
 						 hoverBorderColor: 'lightgrey',
             data: data_selection
           }
         ]
       },
       options: {
         scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          },
         legend: {
           display: false
         },
         title: {
           display: true,
           text: text_label_specific
         }
       }
   });
};
/*
    Takes callback function response argument (JSON data) and builds the main graph
*/
loadJSON(function(response) {
  var category = 'horizontalBar';
  var text_label = 'Channels and Devices for Countries | Hover for statistics, and enjoy the randomized colors';
  var actual_JSON = JSON.parse(response);
  for (var key in actual_JSON) {
    if (!actual_JSON.hasOwnProperty(key)) continue;
    var obj = actual_JSON[key];
    labels.push(key);
    var sets = labels.map(function(item) {
        return [item + ", devices", "channels"];
      }).reduce(function(labels, b) {
        return labels.concat(b);
    });
    for (var prop in obj) {
    if(!obj.hasOwnProperty(prop)) continue;
     datas.push(obj[prop]);
    }
  }
  /*
      Creates dropdown based on Country values from JSON data
  */
  var sel = document.getElementById('CountryList');
  for(var i = 0; i < labels.length; i++) {
      var opt = document.createElement('option');
      opt.innerHTML = labels[i];
      opt.value = labels[i];
      sel.appendChild(opt);
  }
  /*
      Builds "Total devices and channels", builds array objects for stacked horizontal bar chart
  */
  let even = [];
  let odd = [];
  for(var i = 0; i < datas.length; i += 2) { even.push(datas[i]) }
  for(var i = 1; i < datas.length; i += 2) { odd.push(datas[i]) }
  var even_sum = even.reduce((a, b) => a + b, 0);
  var odd_sum = odd.reduce((a, b) => a + b, 0);
  var totals = [ "Totals: " + even_sum + " devices", " and " + odd_sum + " channels."]

  function displayDetails() {
  document.getElementById("details").innerHTML = this;
  }

  totals.showDetails = displayDetails;
  totals.showDetails();
  /*
      Main horizontal bar graph from ChartJS class, takes the data we've organized and implements

      NOTE One of the next things I would do is possible restructure the Main Graph to enable responsiveness,
            everything else is working for small screens
  */
  new Chart(document.getElementById("bar-chart-horizontal"), {
      type: category,
      data: {
        labels: labels,
        datasets: [
          {
            label: 'Devices',
            data: even,
						backgroundColor: "rgba(55, 160, 225, 0.7)",
						hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
						hoverBorderWidth: 2,
						hoverBorderColor: 'lightgrey'
        },
        {
            label: 'Channels',
            data: odd,
						backgroundColor: "rgba(225, 58, 55, 0.7)",
						hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
						hoverBorderWidth: 2,
						hoverBorderColor: 'lightgrey'
        },
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              stacked: true,
              fontSize: 10
            }
          }],
          xAxes: [{
          	stacked: true,
            gridLines: {
              display: false
            },
          }],
        },
        maintainAspectRatio: true,
        legend: {
          display: false
        },
        title: {
          display: true,
          text: text_label
        }
      }
  });
});
